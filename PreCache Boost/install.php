<?php
$this->load->model('extension/modification');

if (VERSION === '2.0.0.0')
    $installDir = DIR_DOWNLOAD . str_replace(array('../', '..\\', '..'), '', $this->request->post['path']);
else
    $installDir = DIR_UPLOAD . str_replace(array('../', '..\\', '..'), '', $this->request->post['path']);

$ocmodDir = $installDir . '/_ocmod';
$ocmodFiles = scandir($ocmodDir);

foreach ($ocmodFiles as $file) {
    if (in_array($file, array('.', '..')))
        continue;

    $xmlFile = $ocmodDir . DIRECTORY_SEPARATOR . $file;
    $xmlContent = file_get_contents($xmlFile);

    if (VERSION > '2.0.0.0')
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->loadXml($xmlContent);
        $codeBlock = $dom->getElementsByTagName('code')->item(0);

        if ($codeBlock)
        {
            $code = $codeBlock->nodeValue;
            $modification = $this->model_extension_modification->getModificationByCode($code);

            if (!empty($modification))
                $this->model_extension_modification->deleteModification($modification['modification_id']);
        }
    }

    file_put_contents($installDir . '/install.xml', $xmlContent);

    $this->xml();
}
